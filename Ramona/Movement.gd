extends RigidBody

# These are the different states the character can be in.
const STATE_Move 	= 0
const STATE_Jump 	= 1
const STATE_Land 	= 2
const STATE_Death 	= 3
const STATE_Use 	= 4
const STATE_Magic 	= 5
const STATE_Super 	= 6
const STATE_Attack 	= 9

const max_speed = 7.5
const accelvel  = 10.0
const deaccelvel= 15.0

const JUMP_Force = 10.0

onready var playerState = STATE_Move
var camera
var player
var animTreePlayer
var space
var _attackPressed = false

var joystickX = 0.0
var joystickZ = 0.0

var moveSpeed = 0.0
var onfloor = false
var doJump  = false
var verticalVelocity = 0.0

const ACTION_None 		= 0
const ACTION_Use 		= 1
const ACTION_Attack 	= 2
const ACTION_Magic 		= 3
const ACTION_Super 		= 4

onready var currentAction = ACTION_None

func do_anim_finished():
	if(currentAction == ACTION_Attack && _attackPressed):
		playerState = STATE_Move
		currentAction = ACTION_Attack
	elif(currentAction != ACTION_None):
		currentAction = ACTION_None

func _ready():
	player = get_node("Player")
	player.connect("AnimationFinished", self,"do_anim_finished")
	camera = get_node("target/Camera")
	set_process(true)
	set_process_input(true)

func _process(delta):
	#update_gui()
	update_animations(delta)
	#respawn()

func update_animations(delta):
	var blending = clamp((2 * moveSpeed/max_speed) - 1.0 , -1.0, 1.0)
	var newPLayerState = STATE_Move
	player.set_move_blending(blending)

	if(onfloor):
		if(currentAction != ACTION_None):
			if(currentAction == ACTION_Attack):
				newPLayerState = STATE_Attack
			if(currentAction == ACTION_Use):
				newPLayerState = STATE_Use
			if(currentAction == ACTION_Magic):
				newPLayerState = STATE_Magic
			if(currentAction == ACTION_Super):
				newPLayerState = STATE_Super
		else:
			newPLayerState = STATE_Move
			pass
	else:
		if(verticalVelocity > 0):
			newPLayerState = STATE_Jump
		else:
			if(abs(verticalVelocity) > 0.75):
				newPLayerState = STATE_Land
			else:
				newPLayerState = playerState

	if(playerState != newPLayerState):
		player.do_action(newPLayerState)
		playerState = newPLayerState

func _integrate_forces(state):
	var xMove = 0.0
	var zMove = 0.0
	var current_velocity = state.get_linear_velocity()
	var mtx = state.get_transform()
	var camMove = mtx.basis[0]
	var currentPosition = mtx.origin
	var cameraPos = camera.get_global_transform().origin
	var delta = state.get_step()
	var g = state.get_total_gravity()
	var up = -g.normalized() 
	
	current_velocity += g*delta
	verticalVelocity = up.dot(current_velocity)
	current_velocity -= up*verticalVelocity
	
	var gp = currentPosition
	var targetPos = gp + Vector3(0, -1.0, 0.0)
	#FIX: move a little up the vector, when using trimesh collision is not detected
	gp.y += 0.5
	var result = state.get_space_state().intersect_ray( gp, targetPos, [ self ] )
	
	onfloor = false
	#If performing action set onfloor=true,this way action will not be interrupted if a 
	#change in velocity happens.
	if(currentAction != ACTION_None):
		onfloor = true
	elif(result.size() > 0 && verticalVelocity < 1.0 ):
		onfloor = true

	#Change if on air movement is not allowed
	#if(onfloor):
	zMove += joystickZ
	xMove += joystickX
	if(onfloor && doJump):
		verticalVelocity += JUMP_Force
		onfloor = false
		doJump = false
		update_animations(delta)

	var move = Vector3(xMove,0,zMove)
	moveSpeed = move.length()
	
	if (moveSpeed < 0.1):
		moveSpeed = 0.0
	else:
		var camDir = cameraPos - currentPosition
		camDir.y = 0.0
		camDir = camDir.normalized()
		move = move.normalized()
		camMove = Vector3( (camDir.z * move.x) + (camDir.x * move.z), 0.0, (camDir.z * move.z) - (camDir.x * move.x))
	
	if (moveSpeed > 0.0):
		var rotationCos = camMove.dot( mtx.basis[2])
		var rotationFactor = 0.0
		
		if(rotationCos > -1.0 && rotationCos < 1.0 ):
			rotationFactor = acos(rotationCos)
		elif(rotationCos < 0.0):
			rotationFactor = PI
		
		var moveLeft = 1
		if (camMove.cross(mtx.basis[2]).y < 0):
			moveLeft = -1
		
		rotationFactor *= moveLeft  * 0.2
		
		var newTransform = mtx.rotated(Vector3(0.0, 1.0, 0.0), rotationFactor)
		state.set_transform(newTransform)
		
		#Accel player ...
		moveSpeed = clamp(current_velocity.length() + (accelvel * delta), 0.0, max_speed * moveSpeed)
	else:
		#If not a move request we should deaccel
		if(onfloor):
			moveSpeed = clamp(current_velocity.length() - (deaccelvel * delta), 0.0, max_speed)
		else:
			moveSpeed = clamp(current_velocity.length() - ((deaccelvel/8) * delta), 0.0, max_speed)

	if(player.addImpulse):
		moveSpeed += player.impulseForce
		player.addImpulse = false
	
	move = mtx.basis[2].normalized() * moveSpeed

	move += up*verticalVelocity
	state.set_linear_velocity(move)

func analog_force_change(currentForce, analog):
	if(currentAction != ACTION_None):
		joystickZ = 0.0
		joystickX = 0.0
		return
	if(analog.name == "ControlMovement"):
		joystickX = currentForce.x
		joystickZ = -currentForce.y

func _input(event):
	if(event.is_action_released("Attack")):
		_attackPressed = false
	if(currentAction != ACTION_None):
		joystickZ = 0.0
		joystickX = 0.0
		return
	if(event.is_action_pressed("move_down")):# && onfloor):
		joystickZ = 1.0
	if(event.is_action_pressed("move_up")):# && onfloor):
		joystickZ = -1.0
	if(event.is_action_pressed("move_left")):# && onfloor):
		joystickX = -1.0
	if(event.is_action_pressed("move_right")):# && onfloor):
		joystickX = 1.0
	if(event.is_action_released("move_down") || event.is_action_released("move_up")):
		joystickZ = 0.0
	if(event.is_action_released("move_left") || event.is_action_released("move_right")):
		joystickX = 0.0
	if(event.is_action_pressed("jump") && onfloor):
		doJump = true
	if(event.is_action_pressed("use_item") && onfloor):
		currentAction = ACTION_Use
	if(event.is_action_pressed("Magic") && onfloor):
		currentAction = ACTION_Magic
	if(event.is_action_pressed("super_move") && onfloor):
		currentAction = ACTION_Super
	if(event.is_action_pressed("Attack") && onfloor):
		currentAction = ACTION_Attack
		_attackPressed = true
	pass