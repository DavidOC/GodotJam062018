extends Spatial

var _at
# Called when the node enters the scene tree for the first time.
func _ready():
	_at = get_node("MeshInstance/AnimationTree")

func _on_Button_button_up():
	var shoot = _at.tree_root.get_node("Shoot")
	shoot.start()
	pass # Replace with function body.


func _on_AnimationPlayer_animation_finished():
	FadeText.show_text("Finished animation", 2.0, VALIGN_CENTER, HALIGN_CENTER)
	print(":(")
	pass # Replace with function body.
