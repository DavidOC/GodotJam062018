extends Spatial

var _ramona
var _pad
var _tween

func _ready():
	_ramona = get_node("Ramona")
	_tween = get_node("Tween")
	_pad = get_node("Pad")
	remove_child(_pad)
	_ramona.attach_to_hand(_pad)
	start_action()
	set_process(true)

func _process(delta):
	if(_ramona.action_is_played()==false and _tween.is_active() == false ):
		_pad.visible = false
		_tween.interpolate_callback(self, 10.0, "start_action")
		_tween.start()
		
func start_action():
	_tween.set_active(false)
	_pad.visible = true
	_ramona.what_to_use = 2
	_ramona.do_action(4)

