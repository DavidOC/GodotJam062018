extends Spatial

var _is_on_tile = false
var _emission_value = 0.0
var _overriden_material
var _new_material
var _time_on_tile = 0.0
var _player_node

func _ready():
	_overriden_material = get_node("MeshInstance").material_override
	_new_material = _overriden_material.duplicate()
	get_node("MeshInstance").set_material_override(_new_material)
	set_process(true)

func _process(delta):
	if (_is_on_tile == true):
		_emission_value = clamp(_emission_value + (delta * 0.5), 0.0, 1.0)
	_new_material.emission_energy = _emission_value
	if (_emission_value >= 0.8 and _is_on_tile == true):
		_time_on_tile += delta

	if(_time_on_tile>=2.0):
		_player_node.take_damage(10)
		_time_on_tile = 0.0
	
func _on_Area_body_entered(body):
	if(body is  KinematicBody):
		_player_node = body
		_is_on_tile = true

func _on_Area_body_exited(body):
	if(body is  KinematicBody):
		_time_on_tile = 0.0
		_is_on_tile = false
