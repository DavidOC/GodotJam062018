extends Spatial

var _meshes_factory = preload("res://Scenes/Floor.tscn")
var _roof_factory = preload("res://Scenes/Ceiling.tscn")

func _ready():
	var room  = get_node("Meshes/Floor")
	var _meshes_scene = _meshes_factory.duplicate()
	print(_meshes_scene.can_instance())
	
	var _roof_scene = _meshes_factory.duplicate()
	var x = -480
	while(x <= 480):
		var z = -480
		while(z < 560):
			var pos = Vector3(x, 72, z)
			var mesh_inst = _meshes_scene.instance().get_node(".").duplicate()
			mesh_inst.set_translation(pos)
			room.add_child(mesh_inst)
			parent_elements(mesh_inst)

			var roof_inst = _roof_scene.instance().get_node(".").duplicate()
			pos.y = 72 * 5.3
			roof_inst.set_translation(pos)
			room.add_child(roof_inst)
			parent_elements(roof_inst)

			z += 80
		x += 80


func parent_elements(var node):
	node.set_owner(self)
	for child in node.get_children():
		parent_elements(child)

