extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var _ap = null
var _timer = null

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	_ap = get_node("AnimationPlayer")
	_timer = get_node("Timer")
	_timer.connect("timeout", self, "_on_Timer_timeout")
	pass

func _on_Timer_timeout():
	_ap.play("Hide")
	yield(_ap, "animation_finished" )
	queue_free()