extends Camera
var _offset
onready var _player = get_parent().get_parent()
onready var _target = get_parent()

var _dir = 0.0
var _adjust_angle = 0.0

var _offset_size = 0.0
const TURN_VELOCITY = 600.0

func _ready():
	set_as_toplevel(true)
	set_process(true)
	set_physics_process(true)
	_offset = get_global_transform().origin - _target.get_global_transform().origin
	_offset_size = _offset.length()

func _physics_process(delta):
	var space_state = get_world().get_direct_space_state()
	var target_position = _target.get_global_transform().origin
	
	var test_pos = target_position +( _offset.normalized() * _offset_size)
	var exclude = get_tree().get_nodes_in_group("NotOccluded")
	exclude.append(_player)
	var collision_result = space_state.intersect_ray(target_position, test_pos , exclude)
	if(collision_result.has("collider")):
		_offset = (collision_result.position - target_position) * 0.75 
	else:
		_offset = _offset.normalized() * _offset_size
		#_offset = test_pos - target_position
	
func _process(delta):
	var up = Vector3(0, 1, 0)
	var target = get_parent().get_global_transform().origin
	if(_dir != 0):
		_offset = Basis(up, deg2rad(delta*TURN_VELOCITY*_dir)).xform(_offset)
	#Adjust tilt
	var angle = rad2deg(asin(_offset.y/_offset_size))
	if((angle>=-20 and angle<=60) or (angle<=-20 and _adjust_angle>0) or (angle>=60 and _adjust_angle<0)):
		_offset.y += sin(deg2rad(_adjust_angle)) * _offset_size
	var pos = target + _offset
	look_at_from_position(pos, target, up)
	#_dir -= lerp(_dir, 0.0, 0.001) * delta
	_dir -= (4 * _dir) * delta
	_adjust_angle -= (4 * _adjust_angle) * delta

func set_dir(var dir):
	_dir = dir

func set_adjust_angle(var angle):
	_adjust_angle = clamp(angle * 20, -20.0, 20.0)