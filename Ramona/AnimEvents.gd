extends Spatial

#############################
var _blinking = false
var _BT = 0.3
var _blink_time = 0.0

var _body
var _eyes
var _eyelashes
var _wait_blink = 2
##############################

enum TRANSITIONS{TRANSITION_Move=0,
				TRANSITION_Jump=1,
				TRANSITION_Land=2,
				TRANSITION_Death=3,
				TRANSITION_Use=4,
				TRANSITION_Magic=5,
				TRANSITION_Super1=6,
				TRANSITION_Super2=7,
				TRANSITION_Super3=8,
				TRANSITION_Attack=9}

export var maxAttacks = 2
export(int, "Drink", "Pad", "Button") var what_to_use = 0
#Add impulse when animation is performed
var addImpulse = false
var impulseForce = 0.0
var _charScale
var arrLengths = []
var _countTime
var _targetTime
var _countingTime = false
var _current_transition = TRANSITION_Move
var _countAttack = 0
var _blending = 0.0
var _at = null
var _movement = null
var _onse_shot = null
var _transition = null
var _actions = null
var _doing_action = false

signal AnimationFinished

func _ready():
	set_process(true)
	_charScale = self.get_scale() #Vector3(0.047, 0.047, 0.047)
	_at = get_node("AnimationTree")
	_movement = _at.tree_root.get_node("Movement")
	_onse_shot = _at.tree_root.get_node("OneShot")
	_transition = _at.tree_root.get_node("Transition")
	_actions = _at.tree_root.get_node("Actions")
	_body = get_node("Skeleton/Body")
	_eyes = get_node("Skeleton/default")
	_eyelashes = get_node("Skeleton/Eyelashes")

func _process(delta):
	if(_blinking):
		_blink_time += delta
		var y = 0
		if(_blink_time > _BT) :
			_blink_time = 0
			_blinking = false
			_wait_blink = 3
		else:
			y = (-4 * ( pow(_blink_time,2)/pow(_BT,2) )) + ((4*_blink_time)/_BT)
		_body.set("blend_shapes/Blink_Left", y)
		_body.set("blend_shapes/Blink_Right", y)
		_eyes.set("blend_shapes/Blink_Left", y)
		_eyes.set("blend_shapes/Blink_Right", y)
		_eyelashes.set("blend_shapes/Blink_Left", y)
		_eyelashes.set("blend_shapes/Blink_Right", y)
	else:
		_wait_blink -= delta
		if(_wait_blink <= 0):
			_blinking = true
	check_finished()

func set_move_blending(blending):
	_blending = blending
	if(_current_transition == TRANSITIONS.TRANSITION_Move):
		var anim
		if(_blending>=0.0 and _blending<= 0.15):
			_movement.set_current(0)
		elif(_blending>0.15 and _blending<= 0.95):
			_movement.set_current(1)
		else:
			_movement.set_current(2)
	
func do_action(transition):
	var offsetTransition = 0
	if(transition==TRANSITION_Attack):
		if(_current_transition == TRANSITION_Attack):
			if(_countAttack >= maxAttacks):
				_countAttack = 1
			else:
				_countAttack += 1
		else:
			_countAttack = 1
		_transition.set_current(_countAttack + offsetTransition)
	if(transition == TRANSITIONS.TRANSITION_Death):
		_transition.set_current(3)
	else:
		_transition.set_current(0)
		if(transition == TRANSITIONS.TRANSITION_Use and _onse_shot.is_active() == false):
			_actions.set_current(what_to_use)
			_onse_shot.start()
			_doing_action = true
	_current_transition = transition

#Call this function from animation player to move player on facing direction
func add_impulse(force):
	impulseForce = force
	addImpulse = true

func check_finished():
	if(_onse_shot.is_active()==false and _doing_action == true):
		_doing_action = false
		emit_signal("AnimationFinished")

func attach_to_hand(what_to_attach):
	var attach_point = get_node("Skeleton/HandAttacment")
	attach_point.add_child(what_to_attach)

func action_is_played():
	return _onse_shot.is_active()
	