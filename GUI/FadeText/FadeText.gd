extends Node

const VERT_UP = 1
const VERT_MIDDLE = 2
const VERT_DOWN = 3

const HOR_LEFT = 1
const HOR_CENTER = 2
const HOR_RIGHT = 3

onready var _text_node = get_node("Label")

signal text_displayed

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	_text_node.modulate = Color(1.0, 1.0, 1.0, 0.0)
	pass

func show_text(var text, var time = 1.0, var v_align = VERT_MIDDLE, var h_align = HOR_CENTER):
	var new_text = get_node("Label").duplicate()
	add_child(new_text)
	new_text.add_to_group("FadedText")
	var ap = new_text.get_node("AnimationPlayer")
	var timer = new_text.get_node("Timer")
	
	new_text.set_text(text)

	ap.play("Show")
	timer.set_wait_time(time)
	timer.start()

	#Wait two frames to allow text to be resized
	yield(get_tree(), "idle_frame")
	
	var x = 0.0
	var y = 0.0
	var projectResolution = get_tree().get_root().size
	#var projectResolution = Vector2(ProjectSettings.get("Display/Window/Width"),ProjectSettings.get("Display/Window/Height"))
	var size = new_text.get_size() #* _text_node.get_scale()
	if(v_align == VERT_MIDDLE):
		y = ( projectResolution.y / 2) - (size.y/2)
	if(v_align == VERT_DOWN):
		y =  projectResolution.y - size.y

	if(h_align == HOR_CENTER):
		x = ( projectResolution.x / 2) - (size.x/2)
	if(h_align == HOR_RIGHT):
		x =  projectResolution.x - size.x

	new_text.rect_position = Vector2(x,y)
	
func text_hidden():
	emit_signal("text_displayed")