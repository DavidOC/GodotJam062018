extends KinematicBody

const STATE_Move 	= 0
const STATE_Jump 	= 1
const STATE_Land 	= 2
const STATE_Death 	= 3
const STATE_Use 	= 4
const STATE_Magic 	= 5
const STATE_Super 	= 6
const STATE_Attack 	= 9

var _velocity = Vector3()
var _joystickZ = 0.0
var _joystickX = 0.0
var _onfloor = false
var _camera
var _moveSpeed = 0.0
var _doJump = false
var _player
var _enemy = null
#var _attackPressed = false

const MAX_SPEED = 5.5
const ACCELVEL  = 5.0
const DEACCELVEL= 10.0
const JUMP_Force = 5.0
const SLIDE_ANGLE = 40.0
#Make it an export or a property of weapon
const ATTACK_RANGE = 5.0
var g = Vector3(0.0, -9.8, 0.0)

const ACTION_None 		= 0
const ACTION_Use 		= 1
const ACTION_Attack 	= 2
const ACTION_Magic 		= 3
const ACTION_Super 		= 4

onready var _currentAction = ACTION_None
onready var _playerState = STATE_Move

var _onAirTime = 0.0

var _health = 100

func take_damage(damage):
	_health = clamp(_health - damage, 0 ,100)
	get_node("AudioStreamPlayer").play()

func set_camera_direction(var dir):
	var new_dir = clamp(dir, -1.0, 1.0)
	_camera.set_dir(new_dir)

func set_camera_angle(var angle):
	var new_angle = clamp(angle, -1.0, 1.0)
	_camera.set_adjust_angle(new_angle)

func get_camera():
	return _camera
	
func return_to_title():
	get_tree().change_scene("res://Scenes/Startup.tscn")
	
func do_anim_finished():
	if(_playerState==STATE_Death):
		#This should emit a signal and the scene manage the event ...
		FadeText.show_text("GAME OVER", 5.0, FadeText.VERT_MIDDLE, FadeText.HOR_CENTER)
		FadeText.connect("text_displayed", self,"return_to_title")
	else:
		if(_currentAction == ACTION_Attack):
			_playerState = STATE_Attack
			_currentAction = ACTION_None
		elif(_currentAction != ACTION_None):
			_currentAction = ACTION_None

func _ready():
	_player = get_node("Player")
	_player.connect("AnimationFinished", self,"do_anim_finished")
	
	_camera = get_node("target/Camera")
	set_process_input(true)
	set_process(true)
	set_physics_process(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
	get_node("HealthBar").value = _health
	var blending = clamp((_moveSpeed/MAX_SPEED), 0.0, 1.0)
	var newPLayerState = STATE_Move
	_player.set_move_blending(blending)
	if(_health > 0):
		if(_onfloor):
			if(_currentAction != ACTION_None):
				if(_currentAction == ACTION_Attack):
					newPLayerState = STATE_Attack
				if(_currentAction == ACTION_Use):
					newPLayerState = STATE_Use
				if(_currentAction == ACTION_Magic):
					newPLayerState = STATE_Magic
				if(_currentAction == ACTION_Super):
					newPLayerState = STATE_Super
			else:
				newPLayerState = STATE_Move
				pass
		else:
			if(_velocity.y > 0):
				newPLayerState = STATE_Jump
			else:
				if(abs(_velocity.y) > 4.0):
					newPLayerState = STATE_Land
				else:
					newPLayerState = _playerState
	else:
		newPLayerState = STATE_Death

	if(_playerState != newPLayerState):
		_player.do_action(newPLayerState)
		if(newPLayerState == STATE_Attack):
			do_attack()
		_playerState = newPLayerState

func do_attack():
	var enemies = get_tree().get_nodes_in_group("Enemies")
	var closest = ATTACK_RANGE
	var pos = get_translation()
	if(_enemy == null or (_enemy.get_translation() - pos).length() > ATTACK_RANGE):
		for enemy in enemies:
			if(enemy is KinematicBody) :
				var new_length = (enemy.get_translation() - pos).length()
				if(new_length < closest):
					closest = new_length
					_enemy = enemy
				pass

func _physics_process(delta):
	if (_playerState==STATE_Death):
		return
		
	var xMove = 0.0
	var zMove = 0.0

	var mtx = get_transform()
	var camMove = mtx.basis.z
	var currentPosition = mtx.origin
	var cameraPos = _camera.get_transform().origin
	var up = -g.normalized();

	zMove = _joystickZ
	xMove = _joystickX

	var hv = Vector3(_velocity.x, 0.0, _velocity.z)
	var move = Vector3(xMove, 0.0, zMove)
	_moveSpeed = move.length();

	if (_doJump && _onfloor):
		_doJump = false
		_onfloor = false
		_velocity.y += JUMP_Force;
		FadeText.show_text("Estoy saltando", 1.5, FadeText.VERT_DOWN, FadeText.HOR_CENTER)

	if (_moveSpeed < 0.1):
		_moveSpeed = 0.0
	else :
		var camDir = cameraPos - currentPosition
		camDir.y = 0.0
		camDir = camDir.normalized()
		move = move.normalized()
		camMove = Vector3((camDir.z * move.x) + (camDir.x * move.z), 0.0, (camDir.z * move.z) - (camDir.x * move.x))

	if (_moveSpeed > 0.0):
		adjust_rotation(camMove, delta);
		# Accel player ...
		_moveSpeed =  clamp(hv.length() + (ACCELVEL * delta), 0.0, MAX_SPEED * _moveSpeed);
	else:
		#If not a move request we should deaccel
		_moveSpeed = clamp(hv.length() - (DEACCELVEL * 4 * delta), 0.0, MAX_SPEED)
		
		
	#b) turn to enemy
	if (_enemy != null && _currentAction == ACTION_Attack):
		adjust_rotation((_enemy.GetTranslation() - currentPosition).Normalized(), delta);

	#if (_hasImpulse):
	#	_moveSpeed += _impulseForce
	#	_hasImpulse = false

	mtx = get_global_transform();
	move = mtx.basis.z.normalized() * _moveSpeed
	_velocity.x = move.x
	_velocity.y += (g * delta).y
	_velocity.z = move.z
	move.y = _velocity.y
	var motion = move_and_slide(move, up)
	if(is_on_floor()):
		_velocity.y = 0
		_onfloor = true
		_onAirTime = 0.0
	else:
		_onAirTime += delta;
		if(_onAirTime > 0.3):
			_onfloor = false

func adjust_rotation(var move, var delta):
	var mtx = self.get_global_transform()
	var rotationCos = move.dot( mtx.basis.z)
	var rotationFactor = 0.0
	
	if(rotationCos > -1.0 && rotationCos < 1.0 ):
		rotationFactor = acos(rotationCos)
	elif(rotationCos < 0.0):
		rotationFactor = PI
	var moveLeft = 1 if (move.cross(mtx.basis.z).y < 0) else -1
	rotationFactor *= moveLeft * delta * 5.0 #rotation velocity
	rotate_y(rotationFactor)

func analog_force_change(currentForce, analog):
	if(_currentAction != ACTION_None):
		_joystickZ = 0.0
		_joystickX = 0.0
		return
	if(analog.name == "ControlMovement"):
		_joystickX = currentForce.x
		_joystickZ = -currentForce.y

func _input(event):
	if(event is InputEventMouseMotion ):
		set_camera_direction((event.relative.x * 25)/OS.get_screen_size().x)
		#set_camera_angle((event.relative_y  * 15)/OS.get_screen_size().width)
	if(_currentAction != ACTION_None):
		_joystickZ = 0.0
		_joystickX = 0.0
		return
	if(event.is_action_pressed("move_down")):# && onfloor):
		_joystickZ = 1.0
	if(event.is_action_pressed("move_up")):# && onfloor):
		_joystickZ = -1.0
	if(event.is_action_pressed("move_left")):# && onfloor):
		_joystickX = -1.0
	if(event.is_action_pressed("move_right")):# && onfloor):
		_joystickX = 1.0
	if(event.is_action_released("move_down") || event.is_action_released("move_up")):
		_joystickZ = 0.0
	if(event.is_action_released("move_left") || event.is_action_released("move_right")):
		_joystickX = 0.0
	if(event.is_action_pressed("jump") && _onfloor):
		_doJump = true
	#if(event.is_action_pressed("use_item") && _onfloor):
	#	_currentAction = ACTION_Use
	#if(event.is_action_pressed("Magic") && _onfloor):
	#	_currentAction = ACTION_Magic
	#if(event.is_action_pressed("super_move") && _onfloor):
	#	_currentAction = ACTION_Super
	#if(event.is_action_pressed("Attack") && _onfloor):
	#	_currentAction = ACTION_Attack
		#_attackPressed = true